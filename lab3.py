#!/usr/bin/env python
import sys

import numpy as np
import pandas as pd

# pylint: disable-msg=E0611
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.naive_bayes import MultinomialNB
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.svm import SVC
from sklearn.neighbors import KNeighborsClassifier

from common import describe_data as describe_data
from common import classification_metrics as classification_metrics
from common import test_env as test_env
__all__ = [describe_data, classification_metrics, test_env]


def read_data(file):
    """Return pandas dataFrame read from Excel file"""
    try:
        return pd.read_excel(file)
    except FileNotFoundError:
        sys.exit('ERROR: ' + file + ' not found')


def encode_categorical(df, categorical_columns):
    for column in categorical_columns:
        df[column] = df[column].fillna(value='Missing')
        df = pd.get_dummies(df, prefix=[column], columns=[column],
                            drop_first=True)
    return df


def preprocess_data(df, verbose=False):
    y_column = 'In university after 4 semesters'

    # Features can be excluded by adding column name to list
    drop_columns = ['Recognition']

    categorical_columns = [
        'Faculty',
        'Paid tuition',
        'Study load',
        'Previous school level',
        'Previous school study language',
        'Recognition',
        'Study language',
        'Foreign student'
    ]

    # Handle dependent variable
    if verbose:
        print('Missing y values: ', df[y_column].isna().sum())

    y = df[y_column].values
    # Encode y. Naive solution
    y = np.where(y == 'No', 0, y)
    y = np.where(y == 'Yes', 1, y)
    y = y.astype(float)

    # Drop also dependent variable variable column to leave only features
    drop_columns.append(y_column)
    df = df.drop(labels=drop_columns, axis=1)

    # Remove drop columns for categorical columns just in case
    categorical_columns = [
        i for i in categorical_columns if i not in drop_columns]

    df = encode_categorical(df, categorical_columns)

    # Handle missing data. At this point only exam points should be missing
    # It seems to be easier to fill whole data frame as only particular columns
    if verbose:
        df.fillna(value=0, inplace=True)

    if verbose:
        describe_data.print_nan_counts(df)

    # Return features data frame and dependent variable
    return df, y


def logistic_regression(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    x_train = sc.fit_transform(x_train)
    x_test = sc.transform(x_test)

    clf = LogisticRegression(random_state=0, solver='sag',
                             penalty='l2', max_iter=1000, multi_class='auto')
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'Logistic regression')


def knn_classifier(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    x_train = sc.fit_transform(x_train)
    x_test = sc.transform(x_test)

    clf = KNeighborsClassifier(n_neighbors=10, metric='minkowski', p=2)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'knn')


def svm_classifier(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    sc = StandardScaler()
    x_train = sc.fit_transform(x_train)
    x_test = sc.transform(x_test)

    clf = SVC(kernel='sigmoid', random_state=0,
              gamma=0.2, C=1.0, tol=1e-6, probability=True)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'SVC')


def naive_bayes_classifier(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    clf = MultinomialNB()
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'Naive Bayes')


def decision_tree_classifier(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    clf = DecisionTreeClassifier(random_state=0)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'Decision tree')


def random_forest_classifier(df, y):
    x_train, x_test, y_train, y_test = train_test_split(
        df, y, test_size=0.25, random_state=0)

    clf = RandomForestClassifier(n_estimators=100, random_state=0)
    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    return classification_metrics.print_metrics(y_test, y_pred, 'Random forest')


if __name__ == '__main__':
    modules = ['numpy', 'pandas', 'sklearn']
    test_env.versions(modules)

    students = read_data('data/students.xlsx')

    describe_data.print_overview(
        students, file='results/students_overview.txt')
    describe_data.print_categorical(
        students, file='results/students_categorical_data.txt')

    drop_students = students[(
        students['In university after 4 semesters'] == 'No')]
    describe_data.print_overview(
        drop_students, file='results/drop_students_overview.txt')
    describe_data.print_categorical(
        drop_students, file='results/drop_students_categorical_features.txt')

    students_x, students_y = preprocess_data(students, True)

    logistic_regression(students_x, students_y)
    knn_classifier(students_x, students_y)
    svm_classifier(students_x, students_y)
    naive_bayes_classifier(students_x, students_y)
    decision_tree_classifier(students_x, students_y)
    random_forest_classifier(students_x, students_y)
    print('Done')
